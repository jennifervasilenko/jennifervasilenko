<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('curl_init'))  throw new Exception('CURL PHP extension is required.');
if (!function_exists('json_decode')) throw new Exception('JSON PHP extension is required.');

class Vkontakte
{
		
	protected $api_url = 'http://oauth.vk.com/authorize';
	protected $client_id = '';
	protected $client_secret = '';
	protected $state = '';
	protected $redirect_uri = '';
	protected $scope = '';
	protected $access_token = '';
	
	protected $error_message = FALSE;

	public function __construct($user_config = array())
	{
		$this->CI =& get_instance();		
		$this->CI->config->load('vkontakte');
		
		$config = array(
			'redirect_uri' => urlencode($this->CI->config->item('redirect_uri')),
			'client_id' => $this->CI->config->item('client_id'),
			'client_secret' => $this->CI->config->item('client_secret'),
			'state' => $this->CI->session->userdata('session_id')
        );
		
        // Set all attributes
        $this->redirect_uri		= $config['redirect_uri'];
        $this->client_id    	= $config['client_id'];
        $this->client_secret 	= $config['client_secret'];
        $this->state  			= $config['state'];

		$this->access_token		= $this->CI->session->userdata('access_token');
	}
	
	public function get_login_url()
	{
		return 'https://oauth.vk.com/authorize?client_id='.$this->client_id.'&display=page&redirect_uri='.$this->redirect_uri.'&scope=email&response_type=code&v=5.60';
	}
	
	public function get_user($params)
	{
		return json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
	}
	
	public function request_access_token($params)
	{
		return json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
	}
}