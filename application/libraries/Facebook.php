<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('curl_init'))  throw new Exception('CURL PHP extension is required.');
if (!function_exists('json_decode')) throw new Exception('JSON PHP extension is required.');

class Facebook
{
		
	protected $api_url = 'https://api.facebook.com/';
	protected $client_id = '';
	protected $client_secret = '';
	protected $state = '';
	protected $redirect_uri = '';
	protected $scope = '';
	protected $access_token = '';
	
	protected $error_message = FALSE;

	public function __construct($user_config = array())
	{
		$this->CI =& get_instance();		
		$this->CI->config->load('facebook');
		
		$config = array(
			'redirect_uri' => urlencode($this->CI->config->item('redirect_uri')),
			'client_id' => $this->CI->config->item('client_id'),
			'client_secret' => $this->CI->config->item('client_secret'),
			'state' => $this->CI->session->userdata('session_id')
        );

        // Set all attributes
        $this->redirect_uri		= $config['redirect_uri'];
        $this->client_id    	= $config['client_id'];
        $this->client_secret 	= $config['client_secret'];
        $this->state  			= $config['state'];

		$this->access_token		= $this->CI->session->userdata('access_token');
	}
	
	public function get_login_url()
	{
		return 'https://www.facebook.com/dialog/oauth?client_id='.$this->client_id.'&display=page&redirect_uri='.$this->redirect_uri.'&scope=email&response_type=code';
	}
	
	public function get_user($params)
	{
		return json_decode(file_get_contents('https://graph.facebook.com/me' . '?' .urldecode(http_build_query($params))));

	}
	
	public function request_access_token($params)
	{
		return json_decode(file_get_contents('https://graph.facebook.com/v2.8/oauth/access_token?client_id='.$params['client_id'].'&redirect_uri='.$params['redirect_uri'].'&client_secret='.$params['client_secret'].'&code='.$params['code']));
	}
}