<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('curl_init'))  throw new Exception('CURL PHP extension is required.');
if (!function_exists('json_decode')) throw new Exception('JSON PHP extension is required.');

class Github 
{
		
	protected $api_url = 'https://api.github.com/';
	protected $client_id = '';
	protected $client_secret = '';
	protected $state = '';
	protected $redirect_uri = '';
	protected $scope = '';
	protected $access_token = '';
	
	protected $error_message = FALSE;

	public function __construct($user_config = array())
	{
		$this->CI =& get_instance();		
		$this->CI->config->load('github');
		
		$config = array(
			'redirect_uri' => urlencode($this->CI->config->item('redirect_uri')),
			'client_id' => $this->CI->config->item('client_id'),
			'client_secret' => $this->CI->config->item('client_secret'),
			'state' => $this->CI->session->userdata('session_id'),
			'scope' => $this->CI->config->item('scope')
        );

        $this->redirect_uri		= $config['redirect_uri'];
        $this->client_id    	= $config['client_id'];
        $this->client_secret 	= $config['client_secret'];
        $this->state  			= $config['state'];
        $this->scope 			= $config['scope'];
	}
	
	public function get_login_url()
	{
		return 'https://github.com/login/oauth/authorize?client_id='.$this->client_id.'&state='.$this->state.'&redirect_uri='.$this->redirect_uri.'&scope='.$this->scope;
	}

	public function get_user($params)
	{
		$url = 'https://api.github.com/user?access_token='.$params['access_token'];
		return $this->curl($url);
	}

	public function request_access_token($params)
	{
		$url = 'https://github.com/login/oauth/access_token?client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&code='.$params['code'].'&state='.$this->state.'&scope='.$this->scope ;
		return $this->curl($url);
	}
	
	public function curl($uri, $verb = 'GET', $body = array(), $headers = FALSE)
	{
		$url = (preg_match('#^www|^http|^//#', $uri)) ? $uri : $this->api_url.$uri.'?access_token='.$this->access_token;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");

		if ($headers)
		{
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}

		if (!empty($body))
		{
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
		}

		switch ($verb)
		{
			case 'POST' :
				curl_setopt($ch, CURLOPT_POST, 1);
				break;
			case 'PATCH' :
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
				break;
			case 'PUT' :
				curl_setopt($ch, CURLOPT_PUT, 1);
				break;
			case 'DELETE' :
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			default :
				break;
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		$output = curl_exec($ch);

		if ($headers)
			$result = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		else
			$result = json_decode($output);

		curl_close($ch);

		if (isset($result->message))
		{
			$this->_set_error($result->message);
			return FALSE;
		}

		return $result;
	}

}