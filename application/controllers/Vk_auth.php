<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vk_auth extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('vkontakte');
		$this->load->model('Users');
	}
	
	public function index()
	{
		$url = $this->vkontakte->get_login_url();
        redirect($url, 'location');
	}
    public function auth()
    {
        if(isset($_GET['code'])){
            $params = array(
                'client_id' => $this->config->item('client_id'),
                'client_secret' => $this->config->item('client_secret'),
                'code' => $_GET['code'],
                'redirect_uri' => $this->config->item('redirect_uri')
            );
            $token = $this->vkontakte->request_access_token($params);

        }

        if (isset($token['access_token'])) {
            $params = array(
                'user_id'         => $token['user_id'],
                'email'         => $token['email'],
                'fields'       => 'uid,email,first_name,last_name',
                'access_token' => $token['access_token']
            );

            $userInfo = $this->vkontakte->get_user($params);
            if (isset($userInfo['response'][0]['uid'])) {
                $userInfo = $userInfo['response'][0];
                $userInfo['email']=$token['email'];
                $result = true;

                $condition = ['social_id' => $userInfo['uid'],
                                'email' => $userInfo['email']];
                $user = $this->Users->get_user($condition);
                if(empty($user)){
                    $new_user = [
                        'username' => $userInfo['first_name'].' '.$userInfo['last_name'],
                        'email' => $userInfo['email'],
                        'social_id' => $userInfo['uid']
                    ];
                    $this->Users->add_user($new_user);
                    $user = $this->Users->get_user($condition);
                }
                foreach ($user as $item){
                    $session_data =['user_id'=>$item['id'],
                        'social_id'=>$item['social_id'],
                        'username'=>$item['username']];
                }
                $this->session->set_userdata($session_data);


               redirect(base_url());
            }
        }
    }
}
