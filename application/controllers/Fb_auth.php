<?php

class Fb_auth extends CI_Controller
{

    public $user = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users');
        $this->load->library('facebook');
    }
    public function index()
    {
        $url = $this->facebook->get_login_url();
        redirect($url, 'location');
    }
    public function auth()
    {
        if(isset($_GET['code'])){
            $params = array(
                'client_id' => $this->config->item('client_id'),
                'client_secret' => $this->config->item('client_secret'),
                'code' => $_GET['code'],
                'redirect_uri' => $this->config->item('redirect_uri'));
            $token = $this->facebook->request_access_token($params);
        }

        if (isset($token->access_token)) {
            $params = array(
                'fields'       => 'id,email,first_name,last_name',
                'access_token' => $token->access_token
            );
            
            $userInfo = $this->facebook->get_user($params);
            if (isset($userInfo->id)) {
                $condition = [
                    'social_id' => $userInfo->id];
                $user = $this->Users->get_user($condition);
                if(empty($user)){
                    $new_user = [
                        'username' => $userInfo->first_name .' '.$userInfo->last_name,
                        'email' => $userInfo->email,
                        'social_id' => $userInfo->id
                    ];
                    $this->Users->add_user($new_user);
                    $user = $this->Users->get_user($condition);
                }
                foreach ($user as $item){
                    $session_data =['user_id'=>$item['id'],
                        'social_id'=>$item['social_id'],
                        'username'=>$item['username']];
                    $this->session->set_userdata($session_data);
                }
                redirect(base_url());
            }
        }
    }
}

?>