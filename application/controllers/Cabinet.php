<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabinet extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form', 'url');
        $this->load->library('form_validation', 'session');
        $this->load->model('Users');
        $this->load->model('Posts');
    }

    public function index($id)
    {
        /*load data*/
        if (isset($id)) {
            $condition = ['users.id' => $id];
            $data['user'] = $this->Users->get_user($condition);
            $condition['user_id'] = $id;
            $data['posts'] = $this->Posts->get_posts($condition);
            /*load data*/

            /*load view*/
            $user['user'] = $this->session->userdata();
            $this->load->view('templates/header', $user);
            if(empty($this->session->userdata('user_id'))){
                $this->load->view('login.php');
            }else{
                $this->load->view('cabinet', $data);
            }
            $this->load->view('templates/footer');
            /*load view*/
        }
    }

    public function edit()
    {
            /*upload config*/
            $config['upload_path'] = './images/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            /*upload config*/
            $this->upload->initialize($config);
            $this->upload->do_upload("avatar");
            $data_image = array('upload_data' => $this->upload->data());
            $user_id = $this->input->post('user_id');
            $condition['id'] = $user_id;
            if (isset($data_image)) {
                $data['update_profile'] = [
                    'avatar' => $data_image['upload_data']['file_name'],
                    'email' => $this->input->post('email')];
            } elseif (empty($data_image)) {
                $data['update_profile'] = [
                    'email' => $this->input->post('email')];
            }
            /*update_recipe*/
            $this->Users->edit_user($data['update_profile'], $condition);
            $this->session->set_flashdata('message', 'Обновлено. Спасибо!');

            redirect(base_url('cabinet/' . $user_id));

    }

}