<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChangePost extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form', 'url');
        $this->load->library('form_validation', 'session');
        $this->load->model('Users');
        $this->load->model('Posts');
    }

    public function index($id)
    {
        /*load data*/
        if (isset($id)) {
            $condition = ['posts.id' => $id];
            $data['post'] = $this->Posts->get_posts($condition);
            /*load data*/

            /*load view*/
            $user['user'] = $this->session->userdata();
            $this->load->view('templates/header', $user);
            $this->load->view('change_post', $data);
            $this->load->view('templates/footer');
            /*load view*/
        }
    }

    public function edit()
    {
        /*validate*/
        $this->form_validation->set_rules('title', 'Название', 'required');
        $this->form_validation->set_rules('text', 'Текст', 'required');
        $this->form_validation->set_message('required', 'Поле "%s" обязательно для заполнения!');
        /*validate*/

        /*save data*/
        if ($this->form_validation->run() == TRUE) {
            /*upload config*/
            $config['upload_path'] = './images/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            /*upload config*/
            $this->upload->initialize($config);
            $this->upload->do_upload("photo");
            $data_image = array('upload_data' => $this->upload->data());
            $post_id = $this->input->post('post_id');
            $condition['id'] = $post_id;
            if (isset($data_image)) {
                $data['update_post'] = [
                    'photo' => $data_image['upload_data']['file_name'],
                    'title' => $this->input->post('title'),
                    'text' => $this->input->post('text'),
                    'date' => date("Y-m-d G:i:s")];
            } elseif (empty($data_image)) {
                $data['update_post'] = [
                    'title' => $this->input->post('title'),
                    'text' => $this->input->post('text'),
                    'date' => date("Y-m-d G:i:s")];
            }
            /*update_recipe*/
            $this->Posts->edit_post($data['update_post'], $condition);
            $this->session->set_flashdata('message', 'Обновлено. Спасибо!');
            redirect(base_url('post/' . $post_id));
        }

    }
    public function delete($id)
    {
        if (isset($id)) {
            $conditions = ['id' => $id];
            $this->Posts->delete_post($conditions);
            $this->session->set_flashdata('message', 'Запись удалена.');
            redirect(base_url());
        }
    }
}