<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddPost extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form', 'url');
        $this->load->library('form_validation', 'session');
        $this->load->model('Users');
        $this->load->model('Posts');
    }

    public function index()
    {
        /*validate*/
        $this->form_validation->set_rules('title', 'Название', 'required');
        $this->form_validation->set_rules('text', 'Текст', 'required');
        $this->form_validation->set_message('required', 'Поле "%s" обязательно для заполнения!');
        /*validate*/

        /*upload config*/
        $config['upload_path'] = './images/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        /*upload config*/
        
        /*save data*/
        if ($this->form_validation->run() == TRUE) {
            $this->upload->initialize($config);
            $this->upload->do_upload("photo");
            $data_image = array('upload_data' => $this->upload->data());
            $post = [
                'title' => $this->input->post('title'),
                'photo' => $data_image['upload_data']['file_name'],
                'text' => $this->input->post('text'),
                'user_id' => $this->session->userdata('user_id'),
                'date' => date("Y-m-d G:i:s")];
            $this->Posts->add_post($post);
            $this->session->set_flashdata('message', 'добавлен. Спасибо!');
            redirect(base_url(), 'refresh');
        }
        /*save data*/

        /*load view*/
        $user['user'] = $this->session->userdata();
        $this->load->view('templates/header', $user);
        if (empty($user['user'])) {
            $this->load->view('denied_access');
        } else {
            $this->load->view('add_post');
        }
        $this->load->view('templates/footer');
        /*load view*/
    }
}