<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public $user=null;
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Users');
		$this->load->model('Posts');
	}

	public function index()
	{
		$user['user'] = $this->session->userdata();
		$this->load->view('templates/header', $user);
		if (empty($user)) {
			$this->load->view('login');
		} else {
			$data['posts'] = $this->Posts->get_posts();
			$this->load->view('home', $data);
		}

		$this->load->view('templates/footer');
	}


	public function login(){
		$user['user'] = $this->session->userdata();
		$this->load->view('templates/header', $user);
		$this->load->view('login.php');
		$this->load->view('templates/footer');
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect('home/login');
	}

}
