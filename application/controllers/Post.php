<?php

class Post extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form', 'file', 'url', 'cookie');
        $this->load->library('form_validation', 'session');
        $this->load->model('Posts');
        $this->load->model('Users');

    }

    public function index($id)
    {
        /*load data*/
        if (isset($id)) {
            $condition = ['posts.id' => $id];
            $data['post'] = $this->Posts->get_posts($condition);
            /*load data*/

            /*load view*/
            $user['user'] = $this->session->userdata();
            $this->load->view('templates/header', $user);
            $this->load->view('post', $data);
            $this->load->view('templates/footer');
            /*load view*/
        }
    }

}