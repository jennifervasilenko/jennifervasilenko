<?php

class Users extends CI_Model
{
    public function get_user($condition=null){

        if ($condition != null) {
            $this->db->where($condition);
        }
        return $this->db->get('users')->result_array();

}

    public function edit_user($data, $conditions = null)
    {
        if ($conditions != null) {
            $this->db->where($conditions);
        }
        $this->db->update('users', $data);

    }
    
    public function add_user($data){

        $this->db->insert('users', $data);
    }
}