<?php

class Posts extends CI_Model
{
    public function get_posts($condition = null)
    {

        $this->db->select('posts.*, users.username, users.social_id');
        $this->db->from('posts');

        $this->db->join('users', 'posts.user_id = users.id', 'join');
        if ($condition != null) {
            $this->db->where($condition);
        }
        $this->db->order_by('date', 'DESC');

        $query = $this->db->get();
        return $query->result();
    }

    public function add_post($data)
    {
        $this->db->insert('posts', $data);
    }

    public function edit_post($data, $conditions = null)
    {
        if ($conditions != null) {
            $this->db->where($conditions);
        }
        $this->db->update('posts', $data);

    }

    public function delete_post($conditions = null)
    {
        if ($conditions != null) {
            $this->db->where($conditions);
        }
        $this->db->delete('posts');

    }
}