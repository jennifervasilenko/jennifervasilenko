<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <?php foreach ($posts as $post) { ?>
                    <article>
                        <div class="post-image">
                            <div class="post-heading">
                                <h3><a href="<?= base_url('post/' . $post->id) ?>"><?= $post->title ?></a></h3>
                            </div>
                            <img src="<?= base_url('/images/uploads/' . $post->photo) ?>" alt="">
                        </div>
                        <p><?= $post->text ?></p>
                        <div class="bottom-article">
                            <ul class="meta-post">
                                <li><i class="fa fa-calendar"></i><a href="#">
                                        <time datetime="2014-01-01">
                                            <?= $post->date ?>
                                        </time>
                                    </a></li>
                                <li><i class="fa fa-user"></i><a href="#"><?= $post->username ?></a></li>
                                </li>
                            </ul>
                            <a href="<?= base_url('post/' . $post->id) ?>" class="pull-right">Смотреть полностью <i
                                    class="fa fa-eye"></i></a>
                        </div>
                    </article>
                <? } ?>
            </div>
        </div>
    </div>
</div><!-- /.container -->
