<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php } ?>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <span>Вы можете изменить информацию о себе</span>
                </div>
                <? foreach ($user as $item) { ?>
                    <h6>Заполните все поля</h6>
                    <? echo validation_errors('<span class="error">', '</span>'); ?>
                    <form enctype="multipart/form-data" id="contactform" action="<?= base_url() . 'cabinet/edit' ?>"
                          method="post" class="validateform" name="send-contact">
                        <div class="row">
                            <div class="col-xs-3"><label><h5>Имя пользователя</h5></label></div>
                            <div class="col-xs-9"><input type="text" name="username" class="form-control"
                                                         value="<?= $item['username'] ?>"
                                                         data-rule="maxlen:255"
                                                         data-msg="Please enter at least 255 chars" disabled/ >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3"><label><h5>Email</h5></label></div>
                            <div class="col-xs-9"><input type="text" name="email" class="form-control"
                                                         value="<?= $item['email'] ?>"
                                                         data-rule="maxlen:255"
                                                         data-msg="Please enter at least 255 chars"/></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3"><img src="<?= base_url('/images/uploads/' . $item['avatar']) ?>"
                                                       alt="" style="width: 50px"></div>
                            <div class="col-xs-9"><input type="file" name="avatar"
                                                         value="<?= base_url('/images/uploads/' . $item['avatar']) ?>"
                                                         class="form-control" multiple=" "/>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <input class="form-control" value="<?= $item['id'] ?>" name="user_id" type="hidden">
                            <button id="btn" class="btn btn-theme" type="submit" value="update">
                                <i
                                    class="fa fa-edit"></i>Изменить
                            </button>

                        </div>

                    </form>
                <? } ?>
                <div>
                    <h2>Ваши посты</h2>
                    <p>Все довленные вами посты</p>
                    <table class="table table-bordered">
                        <tbody>
                        <? foreach ($posts as $post) { ?>
                            <tr>
                                <td width="70%"><?= $post->title ?></td>
                                <td><a href="<?= base_url('post/' . $post->id) ?>">Смотреть полностью <i
                                            class="fa fa-eye"></i></a></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div><!-- /.container -->
