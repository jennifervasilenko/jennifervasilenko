<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>css/style.css" rel="stylesheet">
    <link href="https://lipis.github.io/bootstrap-social/assets/css/font-awesome.css" rel="stylesheet">
    <link href="https://lipis.github.io/bootstrap-social/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://vk.com/js/api/openapi.js?136" type="text/javascript"></script>
    
</head>

<body>
<div id="wrapper">
    <!-- start header -->

    <header>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?=base_url();?>">Home</a></li>
                <li><a href="<?= base_url()?>addPost">Создать</a></li>
            </ul>
            <div class="navbar-form navbar-right">
                <?if(isset($user['user_id'])){?>
                    <a class="links" href="<?= base_url('cabinet/'.$user['user_id']) ?>"><?= $user['username'] ?></a>
                    <a class="links" href="<?= base_url()?>home/logout">Quit</a>
                <?}else{?>
                    <a class="links" href="<?= base_url('home/login')?>">Регистрация через социальные сети</a>
                <?}?>
            </div>
        </div><!--/.nav-collapse -->
    </div>

</div>

</header>

