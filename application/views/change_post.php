<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php } ?>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <span>Добавление новой статьи</span>
                </div>
                <? foreach ($post as $item) { ?>
                    <h6>Заполните все поля</h6>
                    <? echo validation_errors('<span class="error">', '</span>'); ?>
                    <form enctype="multipart/form-data" id="contactform" action="<?= base_url('changePost/edit') ?>"
                          method="post" class="validateform" name="send-contact">
                        <div class="row">
                            <div class="col-xs-3"><label><h5>Введите название</h5></label></div>
                            <div class="col-xs-9"><input type="text" name="title" class="form-control"
                                                         value="<?= $item->title ?>"
                                                         data-rule="maxlen:255"
                                                         data-msg="Please enter at least 255 chars"/></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3"><img src="<?= base_url('/images/uploads/' . $item->photo) ?>" alt="" style="width: 150px"></div>
                            <div class="col-xs-9"><input type="file" name="photo" value="<?= base_url('/images/uploads/' . $item->photo) ?>" class="form-control" multiple=" "/>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-xs-3"><label><h5>Введите текст</h5></label></div>
                            <div class="col-xs-9"><textarea rows="6" name="text" class="form-control"
                                                            data-rule="required"
                                                            data-msg="Please write something"><?= $item->text ?></textarea></div>

                        </div>
                        <div class="col-md-12">
                            <input class="form-control" value="<?= $item->id ?>" name="post_id" type="hidden">
                            <button id="btn" class="btn btn-theme" type="submit" value="do_upload">
                                <i
                                    class="fa fa-edit"></i>Изменить
                            </button>
                            <button id="btn" href="javascript:;" class="btn btn-theme" onClick="confirm_delete(<?= $item->id ?>)"><i
                                    class="fa fa-trash"></i>Удалить</button>
                        </div>

                    </form>
                <? } ?>
            </div>
        </div>
    </div>
    <script>
        function confirm_delete(id) {
            var res = confirm("Точно удалить?");
            if (res == true) {
                window.location.href = "<?=base_url('changePost/delete')?>/" + id;
            }
        }
    </script>
</div><!-- /.container -->
