<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <article>
                    <? foreach ($post as $item){ ?>
                    <div class="post-image">
                        <div class="post-heading">
                            <h2><a href="#"><?= $item->title ?></a></h2>
                        </div>
                        <img class="post_img" src="<?= base_url('/images/uploads/' . $item->photo) ?>" alt="">
                    </div>
                    <p><?= $item->text ?></p>
                    <div class="bottom-article">
                        <ul class="meta-post pull-right">
                            <li><i class="fa fa-calendar"></i><a href="#">
                                    <time datetime="2014-01-01">
                                        <?= $item->date ?>
                                    </time>
                                </a></li>

                            <li><i class="fa fa-user"></i><a href="#"><?= $item->name ?></a></li>
                            <? if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $item->user_id) { ?>
                                <li><a href="<?= base_url('changePost/' . $item->id) ?>">Редактировать<i
                                            class="fa fa-edit"></i></a></li>
                                <li><a href="javascript:;" onClick="confirm_delete(<?= $item->id ?>)"><i
                                            class="fa fa-trash"></i>Удалить</a></a></li>
                            <? } ?>
                        </ul>
                        <? } ?>
                </article>
            </div>
        </div>
    </div>
    <script>
        function confirm_delete(id) {
            var res = confirm("Точно удалить?");
            if (res == true) {
                window.location.href = "<?=base_url('changePost/delete')?>/" + id;
            }
        }
    </script>
</div><!-- /.container -->
