<?php if($this->session->flashdata('message')){?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('message')?>
    </div> <?php } ?>
<div class="container">
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="title">
                <span >Добавление новой статьи</span>
            </div>
            <h6>Заполните все поля</h6>
            <? echo validation_errors('<span class="error">', '</span>'); ?>
            <form enctype="multipart/form-data" id="contactform" action="<?= base_url('addPost') ?>"
                  method="post" class="validateform" name="send-contact">
                <div class="row">
                    <div class="col-lg-12 field">
                        <label><h5>Введите название</h5></label>
                    </div>
                    <div class="col-lg-12 field">
                        <input type="text" name="title" class="form-control" placeholder="Название"
                               data-rule="maxlen:255"
                               data-msg="Please enter at least 255 chars"/>
                    </div>
                    <div class="col-lg-12 field">
                        <label><h5>Загрузите фотографию</h5></label>
                    </div>
                    <div class="col-lg-12 field">
                        <input type="file" name="photo" class="form-control" multiple=" "/>
                    </div>
                    <div class="col-lg-12 field">
                        <label><h5>Введите текст</h5></label>
                    </div>
                    <div class="col-lg-12 field">
                <textarea rows="12" name="text" class="form-control" placeholder="Текст"
                          data-rule="required" data-msg="Please write something"></textarea>
                    </div>
                    <div class="col-lg-12 field">
                        <button id="btn" class="btn btn-theme" type="submit" value="do_upload">
                            Создать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div><!-- /.container -->
