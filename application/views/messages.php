<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php } ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://lipis.github.io/bootstrap-social/assets/css/font-awesome.css" rel="stylesheet">
    <link href="https://lipis.github.io/bootstrap-social/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="fb-root"></div>

<div class="container">
    <div class="header">

        <h3 class="text-muted">TestWork</h3>
    </div>
    <div class="jumbotron">
        <div class="row">
            <? if (!empty($_SESSION['name'])) { ?>
                <div class="col-lg-12" style="margin-bottom: 15px; ">
                    <form enctype="multipart/form-data" id="contactform" action="<?= base_url('addMessage/index') ?>"
                          method="post" class="validateform" name="send-contact">
                        <label><h4>Enter your message:</h4></label>
                        <textarea name="text" class="form-control" rows="3"></textarea>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>
                </div>
            <? } else { ?>
                <h3>If you want to seng the message or comment, you must log in. Please, follow this <a
                        href='<?= base_url() . "home/login"; ?>'>link.</a></h3>
            <? } ?>
            <? foreach ($messages as $message) { ?>
            <div class="col-lg-12">
                <div class="row">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="col-lg-3">
                                <h6><i class="fa fa-calendar"></i>
                                    <time datetime="2014-01-01"><?= $message->date ?></time>
                                </h6>
                            </div>
                            <div class="col-lg-3"><h6><i class="fa fa-user"></i><?= $message->name ?></h6></div>

                            <div class="col-lg-12"><p><?= $message->text ?></p></div>

                            <?php if (!empty($comments)) { ?>

                            <div class="panel panel-default">
                                <div class="panel-body">

                                        <?php foreach ($comments as $item) { ?>
                                            <? if ($message->id == $item['message_id']) { ?>
                                        <ul id="commentRoot">
                                                <li id="comment<?php echo $item['id'] ?>">
                                                    <div class="commentContent">
                                                        <h6><?php echo $item['name'] ?>
                                                            <span><?php echo $item['date'] ?></span></h6>
                                                        <div class="comment">
                                                            <?php echo $item['comment'] ?>
                                                        </div>
                                                        <a class="reply" href="#comment<?php echo $item['id'] ?>">Ответить</a>
                                                    </div>
                                                    <?php if ($item['subtree']) { ?>

                                                            <?foreach($item['subtree'] as $sub){?>
                                                            <ul id="commentsRoot<?php echo $item['id'] ?>">
                                                                <li id="comment<?php echo $sub['id'] ?>">
                                                                    <div class="commentContent">
                                                                        <h6><?php echo $sub['name'] ?>
                                                                            <span><?php echo $sub['date'] ?></span></h6>
                                                                        <div class="comment">
                                                                            <?php echo $sub['comment'] ?>
                                                                        </div>
                                                                        <a class="reply" href="#comment<?php echo $sub['id'] ?>">Ответить</a>
                                                                    </div>
                                                                    <?php if ($sub['subtree']) { ?>
                                                                        <ul id="commentsRoot<?php echo $item['id'] ?>">
                                                                            <?foreach($sub['subtree'] as $child){?>

                                                                            <?}?>
                                                                        </ul>
                                                                    <?php }else{breack;} ?>
                                                                </li>
                                                            </ul>
                                                        <?}?>
                                                    <?php } ?>
                                                </li>
                                            </ul>
                                            <? }
                                        } ?>

                                </div>
                            </div>
                        </div>

                        <?php }
                        } ?>


                        <? if (!empty($_SESSION['name'])) { ?>
                            <form enctype="multipart/form-data" id="contactform"
                                  action="<?= base_url('addMessage/addComment') ?>">
                            </form>
                        <? } ?>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <script>

    </script>
    <? if (!empty($_SESSION['name'])) { ?>
    <a href='<?= base_url() . "home/logout"; ?>'>Quit</a><? } ?>
</div>


<div class="footer" align="center">
    <p>Jennifer Vasilenko for LightIT</p>
</div>


</div>
</body>
</html>