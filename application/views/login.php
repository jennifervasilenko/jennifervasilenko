<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-3">

                <a class="btn btn-block btn-social btn-facebook"
                   href='<?= base_url() . "Fb_auth"; ?>'><span
                        class="fa fa-facebook"></span>Sign in with Facebook</a>
            </div>
            <div class="col-lg-3">
                <a class="btn btn-block btn-social btn-vk" href='<?= base_url() . "Vk_auth"; ?>'><span
                        class="fa fa-vk"></span>Sign in with VK</a>
            </div>
            <div class="col-lg-3">
                <a class="btn btn-block btn-social btn-twitter" href='<?= base_url() . "Twitter_auth"; ?>'><span
                        class="fa fa-twitter"></span>Sign in with Twitter</a>
            </div>
            <div class="col-lg-3">
                <a class="btn btn-block btn-social btn-github" href='<?= base_url() . "Github_auth"; ?>'><span
                        class="fa fa-github"></span>Sign in with GitHub</a>
            </div>
        </div>
    </div>
</div>
</div><!-- /.container -->
